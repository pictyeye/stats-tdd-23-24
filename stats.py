import pytest

class Module:
    def __init__(self):
        self.__grades = {}

    def add_student(self, last_name, first_name, grades):
        self.__grades[(last_name, first_name)] = grades

    def n_students(self):
        return len(self.__grades)

    def is_enrolled(self, last_name, first_name):
        return (last_name, first_name) in self.__grades

@pytest.fixture
def sample_module():
    m = Module()
    m.add_student("Durden", "Tyler", [15, 10, 12])
    m.add_student("Singer", "Marla", [12, 14, 5])
    m.add_student("Paulson", "Bob", [5, 6, 10])
    m.add_student("Face", "Angel", [12, 20, 14])
    return m

def test_n_students(sample_module):
    assert sample_module.n_students() == 4

def test_is_enrolled(sample_module):
    assert sample_module.is_enrolled("Durden", "Tyler")
    assert not sample_module.is_enrolled("Simpson", "Homer")
